#include <IRremote.h>  
#include <Wire.h> 
#include "commands.h"
#include "common.h"

/* ____________________________________________________________________________

    Preprocessor-defined Symbols
   ____________________________________________________________________________
*/
//Pin definitions
#define IR_SEND_PIN             6
#define SHOOT_BUTTON_PIN        11
#define RELOAD_BUTTON_PIN       12
#define SHOOT_INTERRUPT_PIN     9
#define MESSAGE_INTERRUPT_PIN   13

#define I2C_ADDRESS             18          //I2C Address of current module. Default value is 18.

#define DEBUG                   0           //Debug flag.

//Buffer for storing I2C data
byte valueToSend[I2C_BUFFER_SIZE] = "";

//Game information - If weapons is activated it should be read values of the buttons. If not, the buttons are disabled
bool isActivated = false;
bool isOutOfAmmo = false;

volatile byte lastShootButtonState = LOW;   //Last stage of Shoot button
volatile byte lastReloadButtonState = LOW;  //Last stage of Reload button
uint8_t playerID = 0;

/* ____________________________________________________________________________

    void handleReceive(int bytesToRead)

    Function for receiving I2C messages. Reads all data and does an action 
    based on received command in first byte of the message.
   ____________________________________________________________________________
*/
void handleReceive(int bytesToRead) {
  //Read all bytes from I2C
  byte receivedData[I2C_BUFFER_SIZE] = "";
  int readByte = 0;
  
  while (Wire.available()) {
    byte c = Wire.read();

    if (c != 0x00) {
      receivedData[readByte] = c; 
      readByte++;
    }
  }

  //Do an action
  if (readByte > 0) {
    switch (receivedData[0]) {
      case HELLO:   
        valueToSend[0] = HELLO_RESPONSE;
        break;
      case DEACTIVATE:
        isActivated = false;
        break;
      case ACTIVATE:
        isOutOfAmmo = false;
        isActivated = true;
        break;
      case INIT:
        playerID = receivedData[2];
        break;
      case OUTOFAMMO:
        isOutOfAmmo = true;
        break;
      case RELOAD_COMPLETE:
        isOutOfAmmo = false;
        break;
      default:
        debug("Got unknown command", true);
    }
  }

  //Debug
  debug("Read bytes: ", false); debug(readByte, true);
  debug("Command value: ", false); debug((int) receivedData[0], true);
  debug("Sending command value: ", false); debug((int) valueToSend[0], true);  
}

/* ____________________________________________________________________________

    void handleRequest()

    Function for handling request for I2C data. Sends value from valueToSend 
    buffer and sets it to NOINFO action.
   ____________________________________________________________________________
*/
void handleRequest() {
  debug("Got request for data", true);

  //Write all data from valueToSend buffer;
  for (int i = 0; i < I2C_BUFFER_SIZE; i++) {
    Wire.write(valueToSend[i]);
  }
  
  //Empty the buffer and set it to NOINFO command.
  memset(valueToSend, 0, sizeof(valueToSend));
  valueToSend[0] = NOINFO;

  //debug
  debug("Setting command to NOINFO.", true);
}

/* ____________________________________________________________________________

    void sendInterrupt(int pin)

    This function quickly (for 100ms) triggers given pin.
   ____________________________________________________________________________
*/
void sendInterrupt(int pin) {
  pinMode(pin, OUTPUT); //Set to OUTPUT to trigger the bus
  digitalWrite(pin, LOW);
  delay(100);
  digitalWrite(pin, HIGH);
  pinMode(pin, INPUT); //Set back to INPUT to not interfere with the bus
}

/* ____________________________________________________________________________

    void requestI2CSend()

    This function quickly triggers interrupt pin on A9G microcontroller. 
    The microcontroller then asks for the I2C data from all sensors.
   ____________________________________________________________________________
*/
void requestI2CSend() {
  sendInterrupt(MESSAGE_INTERRUPT_PIN);
}

/* ____________________________________________________________________________

    void triggerShoot()

    This function quickly triggers shoot interrupt pin on A9G microcontroller. 
    The microcontrller then registers the shot and if the gun is out of ammo
    then sends OUTOFAMMO message.
   ____________________________________________________________________________
*/
void triggerShoot() {
  sendInterrupt(SHOOT_INTERRUPT_PIN);
}

/* ____________________________________________________________________________

    void debug(char message[], bool ln)

    If preprocessor DEBUG flag is set to 1 then it sends debug messages
    to Serial port. If second parameter is set to true, then it prints
    the message on new line.
   ____________________________________________________________________________
*/
void debug(char message[], bool ln) {
  #if DEBUG >= 1
    if (ln) {
      Serial.println(message);
      return;
    }
    
    Serial.print(message);
  #endif
}

/* ____________________________________________________________________________

    void sendMessageToA9G(char message[])

    This function stores given message into the valueToSend buffer.
    After that requests sending value by calling requestI2CSend()
   ____________________________________________________________________________
*/
void sendMessageToA9G(char message[]) {
  debug("Sending message: ", false);
  debug(message, true);
 
  uint8_t length = (strlen(message) > I2C_BUFFER_SIZE ? I2C_BUFFER_SIZE : strlen(message));
  for (uint8_t i = 0; i < length + 1; i++) { //Add 1 to copy even the null pointer
    valueToSend[i] = message[i]; 
  }

  requestI2CSend();
  debug("Request sent", true);
}


void setup() {
  //Initialize Shoot trigger pin
  pinMode(SHOOT_BUTTON_PIN, INPUT_PULLUP);

  //Initilaize Reload trigger pin
  pinMode(RELOAD_BUTTON_PIN, INPUT_PULLUP);

  // Initialize Interrupt pins - Set to INPUT to not interfere with the bus. It will be set to output when sending data
  pinMode(MESSAGE_INTERRUPT_PIN, INPUT_PULLUP);
  pinMode(SHOOT_INTERRUPT_PIN, INPUT_PULLUP);

  // Initialize IrSender library
  IrSender.begin(IR_SEND_PIN); // Start with IR_SEND_PIN as send pin and if NO_LED_FEEDBACK_CODE is NOT defined, enable feedback LED at default feedback LED pin
  
  // Initialize I2C library
  Wire.begin(I2C_ADDRESS);
  Wire.onRequest(handleRequest);
  Wire.onReceive(handleReceive);

    //Initialize serial and show debug message
  #if DEBUG >= 1
  Serial.begin(9600);
  Serial.print(F("Ready to send IR signals at pin "));
  Serial.println(IR_SEND_PIN);
  #endif
}

void loop() {
  if (isActivated) {
    byte state = digitalRead(SHOOT_BUTTON_PIN);

    if(!isOutOfAmmo) {                                                      //Allow to shoot only if player has ammo
      //Handle debounce and allow only one shot per press.
      if (lastShootButtonState == LOW && state == HIGH) {
        lastShootButtonState = HIGH;
      } else if (lastShootButtonState == HIGH && state == LOW) {
        lastShootButtonState = LOW;
        debug("Pressed trigger, sending value", true);
        uint32_t valueToSend = playerID | BYTE_CHECK_MASK;
    
        IrSender.sendNECRaw(valueToSend, 1);                                //Send ID of this player with IR
        triggerShoot();                                                     //Send information about shot to A9G
      } 
    }
  
    byte reloadState = digitalRead(RELOAD_BUTTON_PIN);
    
    //Handle debounce and allow only one shot per press.
    if (lastReloadButtonState == LOW && reloadState == HIGH) {
      lastReloadButtonState = HIGH;
    } else if (lastReloadButtonState == HIGH && reloadState == LOW) {
      lastReloadButtonState = LOW;
      debug("Pressed reload, sending message interrupt", true);

      char msg[] = {RELOAD, 0x00};                                          //Send reload message to A9G
      sendMessageToA9G(msg);  
    } 
  }
}
