#ifndef COMMANDS_H
#define COMMANDS_H

//A9G -> ARDUINO
#define HELLO           0x01
#define DEACTIVATE      0x02
#define ACTIVATE        0x03
#define OUTOFAMMO       0x04
#define RELOAD_COMPLETE 0x05
#define INIT            0x06


//ARDUINO -> A9G
#define HELLO_RESPONSE  0x81
#define SHOT            0x82
#define RELOAD          0x83
#define NOINFO          0xA0

#endif
